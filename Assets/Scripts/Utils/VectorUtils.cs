﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

public static class VectorUtils
{
    public static float3 ToFloat3(this Vector3 vector) { return new float3(vector.x, vector.y, vector.z); }
    public static float2 ToFloat2(this Vector2 vector) { return new float2(vector.x, vector.y); }
}
