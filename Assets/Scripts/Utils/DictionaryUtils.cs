﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DictionaryUtils
{
    public static List<T> KeyListFromDictionary<T, U>(this Dictionary<T, U> targetDictionary)
    {
        List<T> keys = new List<T>();

        foreach (T t in targetDictionary.Keys)
            keys.Add(t);

        return keys;
    }

    public static void DebugDictionary<T, U>(this Dictionary<T, U> targetDictionary, string dictionaryName = "")
    {
        string output = "Dictionary debug. Click to show results.\n";

        if (!string.IsNullOrWhiteSpace(dictionaryName))
            output += "Name: " + dictionaryName + "\n";

        output += "Count: " + targetDictionary.Count + "\n";

        foreach (T key in targetDictionary.Keys)
        {
            output += "Key: " + key + " - Value: " + targetDictionary[key] + "\n";
        }

        Debug.Log(output);
    }
}
