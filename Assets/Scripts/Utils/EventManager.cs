﻿using System;
using UnityEngine;

public static class EventManager
{
    //GameFlow
    public static Action StartingGame;
    public static Action GameOver;

    //Input
    public static Action<Vector2> TouchBegin;
    public static Action<Vector2> TouchMoved;
    public static Action TouchEnded;
}
