﻿using Unity.Physics;
using Unity.Physics.Authoring;
using UnityEngine.Assertions;
using static Unity.Physics.Material;
using Material = Unity.Physics.Material;


//PROBABLY WE DO NOT NEED THIS SCRIPT. IT IS TEMPORARILY COMMENTED BECAUSE IT CONTAINS unsafe CODE
//UNITY REQUIRES TO ENABLE unsafe CODE FROM PlayerSettings, OTHERWISE THIS CODE GENERATE ERRORS





public static class PhysicsColliderExtensions
{
    //public static Material GetColliderMaterial(this ref PhysicsCollider collider)
    //{
    //    Material material = Unity.Physics.Material.Default;
    //    Assert.IsTrue(collider.Value.Value.CollisionType == CollisionType.Convex);
    //    unsafe
    //    {
    //        var colliderPtr = (ConvexCollider*)collider.ColliderPtr;
    //        material = colliderPtr->Material;
    //    }
    //    return material;
    //}
    //public static void SetColliderMaterial(this ref PhysicsCollider collider, Material newMaterial)
    //{
    //    Assert.IsTrue(collider.Value.Value.CollisionType == CollisionType.Convex);
    //    unsafe
    //    {
    //        var colliderPtr = (ConvexCollider*)collider.ColliderPtr;
    //        colliderPtr->Material = newMaterial;
    //    }
    //}

    //public static void SetColliderMaterialCollisionEvent(this ref PhysicsCollider collider, CollisionResponsePolicy collisionResponse)
    //{
    //    Material material = collider.GetColliderMaterial();

    //    material.CollisionResponse = collisionResponse;

    //    collider.SetColliderMaterial(material);
    //}

    //public static void SetColliderMaterialFriction(this ref PhysicsCollider collider, float friction, Material.CombinePolicy frictionCombinePolicy)
    //{
    //    Material material = collider.GetColliderMaterial();

    //    material.Friction = friction;
    //    material.FrictionCombinePolicy = frictionCombinePolicy;

    //    collider.SetColliderMaterial(material);
    //}

    //public static void SetColliderMaterialRestitution(this ref PhysicsCollider collider, float restitution, Material.CombinePolicy restitutionCombinePolicy)
    //{
    //    Material material = collider.GetColliderMaterial();

    //    material.Restitution = restitution;
    //    material.RestitutionCombinePolicy = restitutionCombinePolicy;        

    //    collider.SetColliderMaterial(material);
    //}
}