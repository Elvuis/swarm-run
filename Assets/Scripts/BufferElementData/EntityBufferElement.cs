﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

public struct EntityBufferElement : IBufferElementData
{
    public Entity Value;    
}
