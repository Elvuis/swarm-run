﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct DistanceFromTargetData : IComponentData
{
    public float Value;    
}
