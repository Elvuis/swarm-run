﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct SpeedData : IComponentData
{
    public float Speed;
    [Tooltip("if true, a random value is picked between 'BaseSpeed - MinOffset' and 'BaseSpeed + MaxOffset'")]
    public bool HasRandomThreshold;
    public float MinOffset;
    public float MaxOffset;
}
