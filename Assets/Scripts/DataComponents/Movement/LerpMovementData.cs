﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct LerpMovementData : IComponentData
{
    public float3 LerpStartingPosition;
    public float3 LerpTargetPosition;
    public float timeElapsed;
}
