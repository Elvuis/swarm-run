﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct CurrentGameMetersPerSecondData : IComponentData
{
    public float CurrentMetersPerSecond;
    [Min(1)]
    public float IncrementFactor;
    [Tooltip("How much time is required before using the Increment Factor (current m/s * Increment Factor)")]
    public float TimeRequiredToUseIncrementFactor;
    public float TimeElapsedFromLastIncrement;
}
