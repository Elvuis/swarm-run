﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct PlayerInputData : IComponentData
{
    [Header("InputMode")]
    public bool KeyboardEnabled;
    public bool TouchEnabled;

    [Header("Keyboard Input")]
    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Right;
    public KeyCode Left;

    [Header("Touch Input")]
    public float2 TouchStartPosition;
}
