﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct GameData : IComponentData
{
    //TODO REFACTORING: multiple smaller IComponentData instead of a single big IComponentData;
    [Header("Swarm Data")]
    public Entity SwarmPivotPrefab;
    public Entity SwarmElementPrefab;
    [Space(10)]
    public float SwarmElementMass;
    public int SwarmQuantity;
    [Min(0.1f)]
    public int BaseMetersPerSecond;

    [Tooltip("In case of collision with an obstacle, if the SwarmForce is higher than the obstacle Mass, the obstacle will be destroied, and a swarm reduction will occour.")]
    [Space(10)]
    [Range(0, 1)]
    public float SwarmReductionFactor;

    [Space(10)]
    public float3 SwarmSpawnPosition;

    [Header("Shop Data")]
    [Tooltip("This price will be multiplied for the current quantity of Swarm Elements")]
    public int SwarmElementBasePrice;
    [Tooltip("This price will be multiplied for the current quantity of Swarm Elements")]
    public int MeterPerSecondBasePrice;

    [Space(5)]
    public int CurrentMoney;
    public int MoneyRewardPerMeter;
}
