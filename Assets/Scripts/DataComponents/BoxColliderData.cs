﻿using System;
using System.Collections;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Authoring;
using UnityEngine;

[GenerateAuthoringComponent]
public struct BoxColliderData : IComponentData 
{
    [Header("Shape Data")]
    public float3 Size;
    public float3 Center;
    public float3 Orientation;
    public float BevelRadius;

    [Header("Material")]
    public CollisionResponsePolicy CollisionResponse;
    public PhysicsMaterialCoefficient Friction;
    public PhysicsMaterialCoefficient Restitution;

    [Header("Collision Filter")]
    public PhysicsCategoryTags BelongsTo;
    public PhysicsCategoryTags CollidesWith;

    [Header("Custom Tags")]
    public CustomPhysicsMaterialTags CustomTags;
}
