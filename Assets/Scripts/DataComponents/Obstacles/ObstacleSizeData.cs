﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct ObstacleSizeData : IComponentData
{
    public float2 Value;
}
