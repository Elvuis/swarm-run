﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

[GenerateAuthoringComponent]
public struct MetersRangePerObstacleSpawnData : IComponentData
{
    public float MinMeters;
    public float MaxMeters;

    [Space(5)]
    public float MetersToSpawnNewObstacle;
}
