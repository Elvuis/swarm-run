﻿using System;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

[GenerateAuthoringComponent]
public struct ObstacleSpawnPositionRangeData : IComponentData
{
    public int2 TopLeftPosition;    
    public int2 BottomRightPosition;
    public float zPosition;
}
