﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class VelocityResetSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.
        WithAll<VelocityResetTag>().
        ForEach((ref PhysicsVelocity physicsVelocity) => {

            physicsVelocity.Linear = float3.zero;
            physicsVelocity.Angular = float3.zero;

        }).ScheduleParallel();
    }
}
