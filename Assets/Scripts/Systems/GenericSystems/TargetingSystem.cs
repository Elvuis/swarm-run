﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateBefore(typeof(MovementSystem))]
public class TargetingSystem : SystemBase
{
    protected override void OnUpdate()
    {
        ComponentDataFromEntity<Translation> allTranslations = GetComponentDataFromEntity<Translation>(true);

        Entities.
            WithReadOnly(allTranslations).
            WithAll<Translation, TargetData, MovementData>().
            ForEach((ref MovementData movementData, in TargetData targetData, in Translation translation) => {

                if (!allTranslations.HasComponent(targetData.Value)) return;

                movementData.MovementDirection = math.normalizesafe(allTranslations[targetData.Value].Value - translation.Value);

            }).ScheduleParallel();
    }
}
