﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Physics;

public class MovementSystem : SystemBase
{
    protected override void OnStartRunning()
    {
        Entities.
        WithoutBurst().
        WithAll<SpeedData>().
        ForEach((ref SpeedData speed) =>
        {
            if (speed.HasRandomThreshold)
            {
                speed.Speed = UnityEngine.Random.Range(speed.Speed - speed.MinOffset, speed.Speed + speed.MaxOffset);
            }

        }).Run();
    }

    protected override void OnUpdate()
    {
        float deltaTime = UnityEngine.Time.deltaTime;

        Entities.
        WithAll<Translation, SpeedData, MovementData>().
        ForEach((ref Translation translation, in SpeedData speed, in MovementData movementData) =>
        {
            if (!movementData.CanMove) return;

            translation.Value += math.normalizesafe(movementData.MovementDirection) * speed.Speed * deltaTime;

        }).ScheduleParallel();
    }
}
