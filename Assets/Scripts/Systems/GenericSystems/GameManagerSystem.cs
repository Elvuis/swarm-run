﻿using System;
using System.Collections.Generic;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

public class GameManagerSystem : SystemBase
{
    #region Jobs
    [BurstCompile]
    private struct HandleGameplayActiveTagJob : IJob
    {
        public EntityCommandBuffer ecb;
        public bool gameplayShouldBeActive;
        public NativeArray<Entity> targetEntity;

        public void Execute()
        {
            if (gameplayShouldBeActive)
            {
                Entity e = ecb.CreateEntity();
                ecb.AddComponent<GameplayActiveTag>(e);
            }
            else
            {
                ecb.DestroyEntity(targetEntity[0]);
            }
        }
    }
    #endregion

    #region SystemBase Methods
    protected override void OnCreate()
    {
        EventManager.StartingGame += OnStartingGame;
        EventManager.GameOver += OnGameOver;
    }

    protected override void OnDestroy()
    {
        EventManager.StartingGame -= OnStartingGame;
        EventManager.GameOver -= OnGameOver;
    }

    protected override void OnUpdate()
    {

    }
    #endregion

    #region Methods
    private void OnStartingGame()
    {
        HandleGameplayActiveTag(true);
    }

    private void OnGameOver()
    {
        HandleGameplayActiveTag(false);
        GainMoney();
    }

    private void HandleGameplayActiveTag(bool setActive)
    {
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
        EntityQuery query = GetEntityQuery(typeof(GameplayActiveTag));
        NativeArray<Entity> targetEntity = query.ToEntityArray(Allocator.TempJob);

        if (!setActive && query.IsEmptyIgnoreFilter)
            Debug.LogError("No entity with 'GameplayActiveTag' found");

        Dependency = new HandleGameplayActiveTagJob()
        {
            ecb = ecb,
            gameplayShouldBeActive = setActive,
            targetEntity = targetEntity
        }.Schedule(Dependency);

        Dependency.Complete();
        ecb.Playback(EntityManager);

        ecb.Dispose();
        targetEntity.Dispose();
    }

    private void GainMoney()
    {
        MetersTraveledData metersTraveled = GetSingleton<MetersTraveledData>();

        Entities.
            WithoutBurst().
            WithAll<GameData>().
            ForEach((ref GameData gameData) =>
            {
                gameData.CurrentMoney += gameData.MoneyRewardPerMeter * (int)metersTraveled.Value;
            }).Run();
    }
    #endregion
}
