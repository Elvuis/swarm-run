﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateAfter(typeof(MetersPerSecondHandlerSystem))]
public class ObstacleMovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        CurrentGameMetersPerSecondData currentGameData = GetSingleton<CurrentGameMetersPerSecondData>();
        float deltaTime = UnityEngine.Time.deltaTime;

        Entities.
            WithAll<ObstacleTag, LerpMovementData>().
            ForEach((ref Translation translation, ref LerpMovementData lerpMovementData, in MovementData movementData) => 
            {
                if(lerpMovementData.timeElapsed == 0)
                {
                    lerpMovementData.LerpStartingPosition = translation.Value;
                    lerpMovementData.LerpTargetPosition = translation.Value + (movementData.MovementDirection * currentGameData.CurrentMetersPerSecond);
                }

                lerpMovementData.timeElapsed += currentGameData.CurrentMetersPerSecond * deltaTime;
                translation.Value = math.lerp(lerpMovementData.LerpStartingPosition, lerpMovementData.LerpTargetPosition, lerpMovementData.timeElapsed);

                if (lerpMovementData.timeElapsed >= 1)
                    lerpMovementData.timeElapsed = 0;
        
            }).ScheduleParallel();
    }
}
