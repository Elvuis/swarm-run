﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

[UpdateInGroup(typeof(FixedStepSimulationSystemGroup))]
[UpdateBefore(typeof(BuildPhysicsWorld))]
public class AddColliderSystem : SystemBase
{
    protected override void OnUpdate()
    {        
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
        var parallelWriter = ecb.AsParallelWriter();

        Entities.
            WithAll<BoxColliderData>().
            WithNone<PhysicsCollider>().
            ForEach((int entityInQueryIndex, Entity entity, in BoxColliderData colliderData, in LocalToWorld localToWorld) =>
            {
                BlobAssetReference<Collider> collider = BoxCollider.Create(new BoxGeometry()
                {
                    Center = float3.zero,
                    Orientation = quaternion.identity,
                    Size = new float3(localToWorld.Right.x, localToWorld.Up.y, localToWorld.Forward.z) * colliderData.Size, //Get the scale of the object and multipy for the chosen size
                    BevelRadius = 0.05f
                }, new CollisionFilter()
                {
                    BelongsTo = colliderData.BelongsTo.Value,
                    CollidesWith = colliderData.CollidesWith.Value
                }, new Material()
                {
                    CollisionResponse = colliderData.CollisionResponse,
                    Friction = colliderData.Friction.Value,
                    FrictionCombinePolicy = colliderData.Friction.CombineMode,
                    Restitution = colliderData.Restitution.Value,
                    RestitutionCombinePolicy = colliderData.Restitution.CombineMode,
                    CustomTags = colliderData.CustomTags.Value
                });

                PhysicsCollider colliderComponent = new PhysicsCollider() { Value = collider };

                parallelWriter.AddComponent(entityInQueryIndex, entity, colliderComponent);
            }).ScheduleParallel();

        Dependency.Complete();

        ecb.Playback(EntityManager);
        ecb.Dispose();
    }
}
