﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateBefore(typeof(MovementSystem))]
public class PlayerInputSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<GameplayActiveTag>();
    }
    protected override void OnUpdate()
    {
        Entities.
            WithoutBurst().
            WithAll<SwarmPivotTag>().
            ForEach((ref MovementData movementData, ref PlayerInputData playerInputData) => 
            {
                movementData.CanMove = true;

                if (playerInputData.KeyboardEnabled)
                {
                    movementData.MovementDirection = float3.zero;

                    if (Input.GetKey(playerInputData.Left)) movementData.MovementDirection.x -= 1f;
                    if (Input.GetKey(playerInputData.Right)) movementData.MovementDirection.x += 1f;
                    if (Input.GetKey(playerInputData.Up)) movementData.MovementDirection.y += 1f;
                    if (Input.GetKey(playerInputData.Down)) movementData.MovementDirection.y -= 1f;
                }

                if (playerInputData.TouchEnabled)
                {
                    if(Input.touchCount > 0)
                    {
                        Touch currentFrameTouch = Input.GetTouch(0);

                        switch (currentFrameTouch.phase)
                        {
                            case TouchPhase.Began:
                                playerInputData.TouchStartPosition = currentFrameTouch.position.ToFloat2();
                                EventManager.TouchBegin?.Invoke(currentFrameTouch.position);
                                break;
                            case TouchPhase.Stationary:
                            case TouchPhase.Moved:
                                movementData.MovementDirection = new float3(currentFrameTouch.position.ToFloat2() - playerInputData.TouchStartPosition, 0);
                                if(currentFrameTouch.phase == TouchPhase.Moved) EventManager.TouchMoved?.Invoke(currentFrameTouch.position);
                                break;
                            case TouchPhase.Ended:
                            case TouchPhase.Canceled:
                                playerInputData.TouchStartPosition = float2.zero;
                                movementData.MovementDirection = float3.zero;
                                EventManager.TouchEnded?.Invoke();
                                break;
                        }
                    }
                }
            }).Run();
    }
}
