﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine;

public class SwarmSpawnerSystem : SystemBase
{
    #region Variables
    GameData gameData;
    #endregion

    #region Jobs
    [BurstCompile]
    private struct SpawnSwarmElementJob : IJob
    {
        public GameData gameData;

        public EntityCommandBuffer entityCommandBuffer;

        public void Execute()
        {
            Entity tmpEntity;
            for (int i = 0; i < gameData.SwarmQuantity; i++)
            {
                tmpEntity = entityCommandBuffer.Instantiate(gameData.SwarmElementPrefab);
                entityCommandBuffer.SetComponent(tmpEntity, new Translation() { Value = gameData.SwarmSpawnPosition });
            }
        }
    }

    [BurstCompile]
    private struct SpawnSwarmJob : IJob
    {
        public Entity swarmEntity;
        public float3 spawnPosition;

        public EntityCommandBuffer entityCommandBuffer;

        public void Execute()
        {
            Entity e = entityCommandBuffer.Instantiate(swarmEntity);
            entityCommandBuffer.SetComponent(e, new Translation() { Value = spawnPosition });
        }
    }

    [BurstCompile]
    private struct DestroySwarmPivotJob : IJob
    {
        public EntityCommandBuffer entityCommandBuffer;
        public NativeArray<Entity> swarmPivot;

        public void Execute()
        {
            for (int i = swarmPivot.Length - 1; i >= 0; i--)
                entityCommandBuffer.DestroyEntity(swarmPivot[i]);
        }
    }
    #endregion

    #region SystemBase Methods
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<GameplayActiveTag>();
    }

    protected override void OnStartRunning()
    {
        gameData = GetSingleton<GameData>();

        SpawnSwarmPivot();
        SpawnSwarmElements();

    }

    protected override void OnUpdate()
    {

    }

    protected override void OnStopRunning()
    {
        DestroySwarmPivot();
    }

    #endregion

    #region Methods    
    private void SpawnSwarmPivot()
    {
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);
        SpawnSwarmJob setSwarmPositionJob = new SpawnSwarmJob()
        {
            swarmEntity = gameData.SwarmPivotPrefab,
            spawnPosition = gameData.SwarmSpawnPosition,
            entityCommandBuffer = ecb
        };

        JobHandle setSwarmSpawnPositionJobHandle = setSwarmPositionJob.Schedule(Dependency);
        Dependency = setSwarmSpawnPositionJobHandle;
        setSwarmSpawnPositionJobHandle.Complete();
        ecb.Playback(EntityManager);
        ecb.Dispose();
    }

    private void SpawnSwarmElements()
    {
        EntityCommandBuffer ecb = new EntityCommandBuffer(Allocator.TempJob);

        //Instantiate and initialise Job
        SpawnSwarmElementJob SpawnJob = new SpawnSwarmElementJob()
        {
            gameData = gameData,
            entityCommandBuffer = ecb
        };

        //Schedule Job
        JobHandle SpawnJobHandle = SpawnJob.Schedule(Dependency);

        //Set SpawnJobHandle as new Dependency.
        Dependency = SpawnJobHandle;

        //Complete Job
        SpawnJobHandle.Complete();

        //Playback
        ecb.Playback(EntityManager);
        ecb.Dispose();

    }

    private void DestroySwarmPivot()
    {
        EntityQuery swarmQuery = GetEntityQuery(typeof(SwarmPivotTag));
        if (swarmQuery.IsEmptyIgnoreFilter) return;

        EntityCommandBuffer entityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);
        NativeArray<Entity> swarmPivot = swarmQuery.ToEntityArray(Allocator.TempJob);

        Dependency = new DestroySwarmPivotJob()
        {
            entityCommandBuffer = entityCommandBuffer,
            swarmPivot = swarmPivot
        }.Schedule(Dependency);

        Dependency.Complete();
        entityCommandBuffer.Playback(EntityManager);
        entityCommandBuffer.Dispose();
        swarmPivot.Dispose();
    }
    #endregion
}
