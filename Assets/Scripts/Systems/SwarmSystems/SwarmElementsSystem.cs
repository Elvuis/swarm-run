﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateBefore(typeof(MovementSystem))]
[UpdateBefore(typeof(TargetingSystem))] //TargetData is set here
[UpdateAfter(typeof(SwarmSpawnerSystem))] //SwarmSpawnerSystem also spawns SwarmElements.
public class SwarmElementsSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<SwarmPivotTag>();
    }

    protected override void OnUpdate()
    {
        EntityQuery query = GetEntityQuery(ComponentType.ReadOnly<SwarmPivotTag>());
        Entity swarmPivot = query.GetSingletonEntity();

        SetTarget(swarmPivot);

        HandleMovementData(swarmPivot);
    }

    private void SetTarget(Entity targetEntity)
    {
        if (targetEntity == Entity.Null) return;

        EntityCommandBuffer entityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);
        var buffer = entityCommandBuffer.AsParallelWriter();

        Entities.
            WithAll<SwarmElementTag>().
            WithNone<TargetData>().
            ForEach((int entityInQueryIndex, Entity entity) =>
            {
                buffer.AddComponent(entityInQueryIndex, entity, new TargetData() { Value = targetEntity });

            }).ScheduleParallel();

        Dependency.Complete();
        entityCommandBuffer.Playback(EntityManager);
        entityCommandBuffer.Dispose();
    }

    private void HandleMovementData(Entity targetEntity)
    {
        Translation targetTranslation = EntityManager.GetComponentData<Translation>(targetEntity);      

        Entities.
            WithAll<SwarmElementTag>().
            ForEach((ref MovementData movementData, in TargetData targetData, in DistanceFromTargetData distanceFromTarget, in Translation translation) =>
            {
                if (targetEntity == Entity.Null)
                {
                    movementData.CanMove = false;
                    return;
                }

                movementData.CanMove = Vector3.Distance(targetTranslation.Value, translation.Value) > distanceFromTarget.Value;

            }).ScheduleParallel();
    }
}
