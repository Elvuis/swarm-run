﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEditor;
using UnityEngine;

[UpdateAfter(typeof(DeathPlaneTriggerSystem))]
public class MetersPerSecondHandlerSystem : SystemBase
{
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<SwarmPivotTag>();
    }

    protected override void OnStartRunning()
    {
        GameData gameData = GetSingleton<GameData>();

        Entities.
            WithAll<SwarmPivotTag>().
            ForEach((ref MetersTraveledData metersTraveled, ref SecondsElapsedData secondsElapsed) =>
            {
                metersTraveled.Value = 0;
                secondsElapsed.Value = 0;
            }).ScheduleParallel();

        float StartingMetersPerSecond = gameData.BaseMetersPerSecond;

        Entities.
            WithAll<CurrentGameMetersPerSecondData>().
            ForEach((ref CurrentGameMetersPerSecondData currentGameData) =>
            {
                currentGameData.CurrentMetersPerSecond = StartingMetersPerSecond;
                currentGameData.TimeElapsedFromLastIncrement = 0;
            }).ScheduleParallel();
    }

    protected override void OnUpdate()
    {
        float deltaTime = UnityEngine.Time.deltaTime;

        Entities.
            WithAll<CurrentGameMetersPerSecondData>().
            ForEach((ref CurrentGameMetersPerSecondData currentGameData) =>
            {
                currentGameData.TimeElapsedFromLastIncrement += deltaTime;
                if(currentGameData.TimeElapsedFromLastIncrement >= currentGameData.TimeRequiredToUseIncrementFactor)
                {
                    currentGameData.TimeElapsedFromLastIncrement = 0;
                    currentGameData.CurrentMetersPerSecond *= currentGameData.IncrementFactor;
                }
            }).ScheduleParallel();

        Dependency.Complete(); //Wait until previous job is completed: If timer expired, we need it to be reset in order to update obstacles speed

        CurrentGameMetersPerSecondData currentGameMetersPerSecondData = GetSingleton<CurrentGameMetersPerSecondData>();

        if(currentGameMetersPerSecondData.TimeElapsedFromLastIncrement == 0)
        {
            Entities.
                WithAll<ObstacleTag, LerpMovementData>().
                ForEach((ref LerpMovementData lerpMovementData, in MovementData movementData) =>
                {
                    lerpMovementData.LerpTargetPosition = lerpMovementData.LerpStartingPosition + (movementData.MovementDirection * currentGameMetersPerSecondData.CurrentMetersPerSecond);
                }).ScheduleParallel();
        }

        Entities.
            WithAll<SwarmPivotTag>().
            ForEach((ref MetersTraveledData metersTraveled, ref SecondsElapsedData secondsElapsed) => 
            {
                secondsElapsed.Value += deltaTime;
                metersTraveled.Value += currentGameMetersPerSecondData.CurrentMetersPerSecond * deltaTime;
            }).ScheduleParallel();
    }
}
