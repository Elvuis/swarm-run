﻿using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;
using UnityEngine.Assertions.Must;

[UpdateBefore(typeof(MovementSystem))]
public class SwarmCollisionSystem : SystemBase
{
    #region Variables
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndFixedStepSimulationEntityCommandBufferSystem EndFixedStepSimulationCommandBuffer;

    GameData gameData; 
    #endregion

    #region Jobs
    [BurstCompile]
    private struct PlayerCollisionJob : ICollisionEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<ObstacleTag> obstacles;
        [ReadOnly] public ComponentDataFromEntity<SwarmPivotTag> swarms;
        [ReadOnly] public ComponentDataFromEntity<MassData> massComponentData;

        public EntityCommandBuffer entityCommandBuffer;
        public float swarmForce;
        public float swarmReductionFactor;
        public NativeArray<Entity> swarmElements;
        public NativeArray<int> currentSwarmElementsCount;
        public NativeArray<bool> collidedAgainstObstacle;

        public void Execute(CollisionEvent collisionEvent)
        {
            Entity obstacleEntity = Entity.Null;
            Entity playerEntity = Entity.Null;

            SetEntities(ref obstacleEntity, ref playerEntity, collisionEvent);

            if (obstacleEntity == Entity.Null || playerEntity == Entity.Null) return;

            collidedAgainstObstacle[0] = true;

            float obstacleMass;
            obstacleMass = massComponentData[obstacleEntity].Value;

            if (obstacleMass > swarmForce)
            {
                DestroySwarmElements(swarmElements.Length);
                currentSwarmElementsCount[0] = 0;
            }
            else
            {
                int swarmElementsLostCount = (int)(obstacleMass * swarmReductionFactor);
                swarmElementsLostCount = math.clamp(swarmElementsLostCount, 0, swarmElements.Length);

                DestroySwarmElements(swarmElementsLostCount);
                currentSwarmElementsCount[0] = swarmElements.Length - swarmElementsLostCount;
                entityCommandBuffer.DestroyEntity(obstacleEntity);
            }
        }

        private void SetEntities(ref Entity entityA, ref Entity entityB, CollisionEvent collisionEvent)
        {
            if (obstacles.HasComponent(collisionEvent.EntityA))
                entityA = collisionEvent.EntityA;
            else if (obstacles.HasComponent(collisionEvent.EntityB))
                entityA = collisionEvent.EntityB;

            if (swarms.HasComponent(collisionEvent.EntityA))
                entityB = collisionEvent.EntityA;
            else if (swarms.HasComponent(collisionEvent.EntityB))
                entityB = collisionEvent.EntityB;
        }

        private void DestroySwarmElements(int quantity)
        {
            int swarmElementsLeftCount = swarmElements.Length - quantity;

            for (int i = swarmElements.Length - 1; i >= swarmElementsLeftCount; i--)
                entityCommandBuffer.DestroyEntity(swarmElements[i]);
        }
    }

    #endregion

    #region SystemBase Methods
    protected override void OnCreate()
    {
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        EndFixedStepSimulationCommandBuffer = World.GetOrCreateSystem<EndFixedStepSimulationEntityCommandBufferSystem>();

        RequireSingletonForUpdate<SwarmPivotTag>();
    }

    protected override void OnStartRunning()
    {
        gameData = GetSingleton<GameData>();
    }

    protected override void OnUpdate()
    {
        ScheduleCollisionJob();
    }

    #endregion

    #region Methods
    private void ScheduleCollisionJob()
    {
        NativeArray<Entity> swarmElements = GetEntityQuery(typeof(SwarmElementTag)).ToEntityArray(Allocator.TempJob);

        NativeArray<int> currentSwarmElementsCount = new NativeArray<int>(1, Allocator.TempJob);
        currentSwarmElementsCount[0] = swarmElements.Length;
        NativeArray<bool> collidedAgainstObstacle = new NativeArray<bool>(1, Allocator.TempJob);
        collidedAgainstObstacle[0] = false;

        float currentMetersPerSecond = GetSingleton<CurrentGameMetersPerSecondData>().CurrentMetersPerSecond;

        #region PlayerCollisionJob
        PlayerCollisionJob collisionJob = new PlayerCollisionJob()
        {
            obstacles = GetComponentDataFromEntity<ObstacleTag>(true),
            swarms = GetComponentDataFromEntity<SwarmPivotTag>(true),
            massComponentData = GetComponentDataFromEntity<MassData>(true),
            entityCommandBuffer = EndFixedStepSimulationCommandBuffer.CreateCommandBuffer(),
            swarmForce = swarmElements.Length * gameData.SwarmElementMass * currentMetersPerSecond,
            swarmReductionFactor = gameData.SwarmReductionFactor,
            swarmElements = swarmElements,
            currentSwarmElementsCount = currentSwarmElementsCount,
            collidedAgainstObstacle = collidedAgainstObstacle
        };

        JobHandle collisionJobHandle = collisionJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency); //Dependency from SystemBase class

        Dependency = collisionJobHandle;
        EndFixedStepSimulationCommandBuffer.AddJobHandleForProducer(Dependency);

        collisionJobHandle.Complete();
        #endregion

        if (collidedAgainstObstacle[0] && currentSwarmElementsCount[0] <= 0)
            EventManager.GameOver?.Invoke();

        collidedAgainstObstacle.Dispose();
        currentSwarmElementsCount.Dispose();
        swarmElements.Dispose();
    }
    #endregion
}
