﻿using System;
using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Transforms;

public class DeathPlaneTriggerSystem : SystemBase
{
    #region Variables
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndFixedStepSimulationEntityCommandBufferSystem EndFixedStepSimulationCommandBuffer;
    #endregion

    #region Jobs
    [BurstCompile]
    private struct ObstacleTriggerOnDeathPlaneJob : ITriggerEventsJob
    {
        [ReadOnly] public ComponentDataFromEntity<ObstacleTag> obstacles;
        [ReadOnly] public ComponentDataFromEntity<DeathPlaneTag> deathPlanes;

        public EntityCommandBuffer entityCommandBuffer;

        public void Execute(TriggerEvent triggerEvent)
        {
            Entity obstacleEntity = Entity.Null;
            Entity deathPlaneEntity = Entity.Null;

            SetEntities(ref obstacleEntity, ref deathPlaneEntity, triggerEvent);

            if (obstacleEntity == Entity.Null || deathPlaneEntity == Entity.Null) return;

            entityCommandBuffer.DestroyEntity(obstacleEntity);
        }

        private void SetEntities(ref Entity entityA, ref Entity entityB, TriggerEvent triggerEvent)
        {
            if (obstacles.HasComponent(triggerEvent.EntityA))
                entityA = triggerEvent.EntityA;
            else if (obstacles.HasComponent(triggerEvent.EntityB))
                entityA = triggerEvent.EntityB;

            if (deathPlanes.HasComponent(triggerEvent.EntityA))
                entityB = triggerEvent.EntityA;
            else if (deathPlanes.HasComponent(triggerEvent.EntityB))
                entityB = triggerEvent.EntityB;
        }
    }
    #endregion

    #region SystemBase Methods
    protected override void OnCreate()
    {
        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        EndFixedStepSimulationCommandBuffer = World.GetOrCreateSystem<EndFixedStepSimulationEntityCommandBufferSystem>();

        RequireSingletonForUpdate<GameplayActiveTag>();
    }

    protected override void OnUpdate()
    {
        ScheduleTriggerJob();
    }

    #endregion

    #region Methods
    private void ScheduleTriggerJob()
    {
        ObstacleTriggerOnDeathPlaneJob TriggerJob = new ObstacleTriggerOnDeathPlaneJob()
        {
            obstacles = GetComponentDataFromEntity<ObstacleTag>(true),
            deathPlanes = GetComponentDataFromEntity<DeathPlaneTag>(true),
            entityCommandBuffer = EndFixedStepSimulationCommandBuffer.CreateCommandBuffer()            
        };

        JobHandle TriggerJobHandle = TriggerJob.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        Dependency = TriggerJobHandle;
        EndFixedStepSimulationCommandBuffer.AddJobHandleForProducer(Dependency);
        TriggerJobHandle.Complete();
    }
    #endregion
}
