﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

[UpdateAfter(typeof(MetersPerSecondHandlerSystem))]
public class ObstacleSpawnerSystem : SystemBase
{
    #region Variables
    EntityQueryDesc obstaclesQueryFilters = new EntityQueryDesc()
    {
        All = new ComponentType[] { typeof(ObstacleTag) },
        None = new ComponentType[] { typeof(MassData), typeof(SpeedData) }
    };
    #endregion

    #region Jobs
    [BurstCompile]
    private struct SpawnObstacleJob : IJob
    {
        public EntityCommandBuffer entityCommandBuffer;
        public DynamicBuffer<EntityBufferElement> obstacleBuffer;
        public int randomObstacle;
        public NativeArray<Entity> spawnedEntity;

        public void Execute()
        {
            Entity e = obstacleBuffer.ElementAt(randomObstacle).Value;
            spawnedEntity[0] = entityCommandBuffer.Instantiate(e);
        }
    }

    [BurstCompile]
    private struct InitObstaclesJob : IJob
    {
        public EntityCommandBuffer entityCommandBuffer;
        public float3 spawnPosition;
        public ObstacleSpawnPositionRangeData spawnPositionsRange;
        public NativeArray<Entity> targetEntity;
        public float metersTraveled;
        public float baseObstacleMass;

        [ReadOnly] public ComponentDataFromEntity<ObstacleSizeData> obstaclesSizes;

        public void Execute()
        {
            SetSpawnPosition();

            SetMass();

            //AddLerpMovementData
            entityCommandBuffer.AddComponent(targetEntity[0], new LerpMovementData() { timeElapsed = 0 });
        }

        private void SetSpawnPosition()
        {
            ObstacleSizeData targetSize = obstaclesSizes[targetEntity[0]];

            //Snap Horizontal
            if (spawnPosition.x + targetSize.Value.x / 2 > spawnPositionsRange.BottomRightPosition.x) //Too much right
                spawnPosition.x -= targetSize.Value.x / 2;
            else if (spawnPosition.x - targetSize.Value.x / 2 < spawnPositionsRange.TopLeftPosition.x) //Too much left
                spawnPosition.x += targetSize.Value.x / 2;

            //Snap Vertical
            if (spawnPosition.y + targetSize.Value.y / 2 > spawnPositionsRange.TopLeftPosition.y) //Too much up
                spawnPosition.y -= targetSize.Value.y / 2;
            else if (spawnPosition.y - targetSize.Value.y / 2 < spawnPositionsRange.BottomRightPosition.y) //Too much down
                spawnPosition.y += targetSize.Value.y / 2;

            entityCommandBuffer.SetComponent(targetEntity[0], new Translation() { Value = spawnPosition });
        }

        private void SetMass()
        {
            float obstacleMass = math.max(baseObstacleMass, baseObstacleMass * (int)metersTraveled); //On start game, metersTraveled = 0
            entityCommandBuffer.AddComponent(targetEntity[0], new MassData() { Value = obstacleMass });
        }
    }

    [BurstCompile]
    private struct DestroyExistingObstaclesJob : IJob
    {
        public EntityCommandBuffer entityCommandBuffer;
        public NativeArray<Entity> obstacles;

        public void Execute()
        {
            for (int i = obstacles.Length - 1; i >= 0; i--)
                entityCommandBuffer.DestroyEntity(obstacles[i]);
        }
    }
    #endregion

    #region SystemBase Methods
    protected override void OnCreate()
    {
        RequireSingletonForUpdate<GameplayActiveTag>();
    }

    protected override void OnStartRunning()
    {
        SpawnRandomObstacle();
    }

    protected override void OnStopRunning()
    {
        DestroyExistingObstacles();
    }

    protected override void OnUpdate()
    {
        //SpawnRandomObstacle
        MetersTraveledData metersTraveled = GetSingleton<MetersTraveledData>();
        MetersRangePerObstacleSpawnData metersRange = GetSingleton<MetersRangePerObstacleSpawnData>();
        if(metersTraveled.Value >= metersRange.MetersToSpawnNewObstacle)
            SpawnRandomObstacle(metersTraveled.Value);

        //Init Obstacle
        InitObstacles();
    }
    #endregion

    #region Methods
    private void SpawnRandomObstacle(float metersTraveled = 0)
    {
        EntityCommandBuffer spawnEntityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);

        Entity obstacleSpawnerEntity = GetSingletonEntity<ObstacleSpawnerTag>();
        DynamicBuffer<EntityBufferElement> obstaclesBuffer = GetBuffer<EntityBufferElement>(obstacleSpawnerEntity);

        NativeArray<Entity> spawnedEntities = new NativeArray<Entity>(1, Allocator.TempJob);
        //Spawn entity
        SpawnObstacleJob spawnObstacleJob = new SpawnObstacleJob()
        {
            entityCommandBuffer = spawnEntityCommandBuffer,
            obstacleBuffer = obstaclesBuffer,
            randomObstacle = UnityEngine.Random.Range(0, obstaclesBuffer.Length),
            spawnedEntity = spawnedEntities
        };
        JobHandle spawnObstacleJobHandle = spawnObstacleJob.Schedule(Dependency);
        Dependency = spawnObstacleJobHandle;
        spawnObstacleJobHandle.Complete();
        spawnEntityCommandBuffer.Playback(EntityManager);
        spawnEntityCommandBuffer.Dispose();
        spawnedEntities.Dispose();

        //Set how much meters are required to spawn a new obstacle
        Entities.
            WithoutBurst().
            WithAll<MetersRangePerObstacleSpawnData>().
            ForEach((ref MetersRangePerObstacleSpawnData metersRangePerObstacleSpawn) =>
            {
                metersRangePerObstacleSpawn.MetersToSpawnNewObstacle = metersTraveled + UnityEngine.Random.Range(metersRangePerObstacleSpawn.MinMeters, metersRangePerObstacleSpawn.MaxMeters);
            }).Run();
    }

    private void InitObstacles()
    {
        EntityQuery obstaclesQuery = GetEntityQuery(obstaclesQueryFilters);
        if (obstaclesQuery.IsEmptyIgnoreFilter) return;

        EntityCommandBuffer initEntityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);
        NativeArray<Entity> obstacles = obstaclesQuery.ToEntityArray(Allocator.TempJob);

        ObstacleSpawnPositionRangeData positionRange = GetSingleton<ObstacleSpawnPositionRangeData>();
        ComponentDataFromEntity<ObstacleSizeData> obstaclesSizes = GetComponentDataFromEntity<ObstacleSizeData>(true);
        MetersTraveledData metersTraveled = GetSingleton<MetersTraveledData>();
        ObstacleBaseMassData obstacleBaseMass = GetSingleton<ObstacleBaseMassData>();

        Dependency = new InitObstaclesJob()
        {
            entityCommandBuffer = initEntityCommandBuffer,
            spawnPosition = new float3()
            {
                x = UnityEngine.Random.Range(positionRange.TopLeftPosition.x, positionRange.BottomRightPosition.x),
                y = UnityEngine.Random.Range(positionRange.TopLeftPosition.y, positionRange.BottomRightPosition.y),
                z = positionRange.zPosition
            },
            spawnPositionsRange = positionRange,
            targetEntity = obstacles,
            obstaclesSizes = obstaclesSizes,
            metersTraveled = metersTraveled.Value,
            baseObstacleMass = obstacleBaseMass.Value,

        }.Schedule(Dependency);
        Dependency.Complete();

        initEntityCommandBuffer.Playback(EntityManager);
        initEntityCommandBuffer.Dispose();
        obstacles.Dispose();

    }

    private void DestroyExistingObstacles()
    {
        EntityQuery obstaclesQuery = GetEntityQuery(typeof(ObstacleTag));

        if (obstaclesQuery.IsEmptyIgnoreFilter) return;

        EntityCommandBuffer entityCommandBuffer = new EntityCommandBuffer(Allocator.TempJob);
        NativeArray<Entity> obstacles = obstaclesQuery.ToEntityArray(Allocator.TempJob);

        Dependency = new DestroyExistingObstaclesJob()
        {
            entityCommandBuffer = entityCommandBuffer,
            obstacles = obstacles
        }.Schedule(Dependency);

        Dependency.Complete();
        entityCommandBuffer.Playback(EntityManager);
        entityCommandBuffer.Dispose();
        obstacles.Dispose();
    }
    #endregion

}
