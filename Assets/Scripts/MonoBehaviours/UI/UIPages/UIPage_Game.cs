﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class UIPage_Game : UIPage
{
    #region Variables
    [SerializeField] protected RectTransform touchBackgroundIMG;
    [SerializeField] protected RectTransform touchDirectionalIMG;
    #endregion

    #region MonoBehaviours
    void OnEnable()
    {
        EventManager.GameOver += OnGameOver;
        EventManager.TouchBegin += OnTouchBegin;
        EventManager.TouchMoved += OnTouchMoved;
        EventManager.TouchEnded += OnTouchEnded;
    }

    void OnDisable()
    {
        EventManager.GameOver -= OnGameOver;
        EventManager.TouchBegin -= OnTouchBegin;
        EventManager.TouchMoved -= OnTouchMoved;
        EventManager.TouchEnded -= OnTouchEnded;
    }
    #endregion

    #region Methods
    private void OnGameOver()
    {
        UIManager.singleInstance.SetPage<UIPage_GameOver>();
        OnTouchEnded();
    }

    private void OnTouchBegin(Vector2 position) 
    {
        touchBackgroundIMG.gameObject.SetActive(true);
        touchDirectionalIMG.gameObject.SetActive(true);

        touchBackgroundIMG.position = new Vector3(position.x, position.y, touchBackgroundIMG.position.z);
        touchDirectionalIMG.position = new Vector3(position.x, position.y, touchDirectionalIMG.position.z);
    }
    private void OnTouchMoved(Vector2 position) 
    {
        float positionX = Mathf.Clamp(position.x, touchBackgroundIMG.position.x - touchBackgroundIMG.rect.width / 2, touchBackgroundIMG.position.x + touchBackgroundIMG.rect.width / 2);
        float positionY = Mathf.Clamp(position.y, touchBackgroundIMG.position.y - touchBackgroundIMG.rect.height / 2, touchBackgroundIMG.position.y + touchBackgroundIMG.rect.height / 2);
        touchDirectionalIMG.position = new Vector3(positionX, positionY, touchDirectionalIMG.position.z);
    }
    private void OnTouchEnded()
    {
        touchBackgroundIMG.gameObject.SetActive(false);
        touchDirectionalIMG.gameObject.SetActive(false);
    }
    #endregion
}
