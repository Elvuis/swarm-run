﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Entities;
using Unity.Collections;
using UnityEngine.UI;

public class UIPage_Shop : UIPage
{
    #region Variables
    [SerializeField] protected TextMeshProUGUI CurrentMetersPerSecondText;
    [SerializeField] protected TextMeshProUGUI CurrentSwarmElementsText;
    [SerializeField] protected TextMeshProUGUI CurrentMoneyText;
    [SerializeField] protected TextMeshProUGUI NextSwarmElementPriceText;
    [SerializeField] protected TextMeshProUGUI NextMetersPerSecondsPriceText;

    [Space(5)]
    [SerializeField] protected Button BuySwarmElementButton;
    [SerializeField] protected Button BuyMeterPerSecondButton;

    EntityManager entityManager;
    #endregion

    #region MonoBehaviours
    private void Awake()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    private void OnEnable()
    {
        InitShopPage();
    }

    private void OnDisable()
    {
        
    }
    #endregion

    #region Methods
    private void InitShopPage()
    {
        GameData gameData = GetGameDataComponent();
        UpdateTexts(gameData);
        UpdateButtonsState(gameData);
    }

    private void UpdateTexts(GameData gameData)
    {
        CurrentMetersPerSecondText.text = gameData.BaseMetersPerSecond.ToString();
        CurrentSwarmElementsText.text = gameData.SwarmQuantity.ToString();
        CurrentMoneyText.text = gameData.CurrentMoney.ToString() + "$";

        NextSwarmElementPriceText.text = (gameData.SwarmElementBasePrice * gameData.SwarmQuantity).ToString() + "$";
        NextMetersPerSecondsPriceText.text = (gameData.MeterPerSecondBasePrice * gameData.BaseMetersPerSecond).ToString() + "$";
    }

    public void BackToMainMenuPage()
    {
        UIManager.singleInstance.SetPage<UIPage_MainMenu>();
    }

    public void BuySwarmElement()
    {
        EntityQuery query = entityManager.CreateEntityQuery(typeof(GameDataContainerTag));
        Entity e = query.GetSingletonEntity();

        GameData gameData = GetGameDataComponent();

        //WAITING FOR GAMEDATA SCRIPT REFACTORING INTO SMALLER COMPONENTS
        entityManager.SetComponentData(e, new GameData()
        {
            //Copy old values
            SwarmPivotPrefab = gameData.SwarmPivotPrefab,
            SwarmElementPrefab = gameData.SwarmElementPrefab,
            SwarmElementMass = gameData.SwarmElementMass,
            BaseMetersPerSecond = gameData.BaseMetersPerSecond,
            SwarmReductionFactor = gameData.SwarmReductionFactor,
            SwarmSpawnPosition = gameData.SwarmSpawnPosition,
            SwarmElementBasePrice = gameData.SwarmElementBasePrice,
            MeterPerSecondBasePrice = gameData.MeterPerSecondBasePrice,
            //Set new values //SHOULD BE DONE BY A ShopSystem
            CurrentMoney = gameData.CurrentMoney - (gameData.SwarmElementBasePrice * gameData.SwarmQuantity),
            SwarmQuantity = gameData.SwarmQuantity + 1,
        });

        gameData = GetGameDataComponent();//GetComponentData is called again because it was updated

        UpdateTexts(gameData); 
        UpdateButtonsState(gameData);
    }

    public void BuyMeterPerSecond()
    {
        EntityQuery query = entityManager.CreateEntityQuery(typeof(GameDataContainerTag));
        Entity e = query.GetSingletonEntity();

        GameData gameData = GetGameDataComponent();

        //WAITING FOR GAMEDATA SCRIPT REFACTORING INTO SMALLER COMPONENTS
        entityManager.SetComponentData(e, new GameData()
        {
            //Copy old values
            SwarmPivotPrefab = gameData.SwarmPivotPrefab,
            SwarmElementPrefab = gameData.SwarmElementPrefab,
            SwarmElementMass = gameData.SwarmElementMass,
            SwarmQuantity = gameData.SwarmQuantity,
            SwarmReductionFactor = gameData.SwarmReductionFactor,
            SwarmSpawnPosition = gameData.SwarmSpawnPosition,
            SwarmElementBasePrice = gameData.SwarmElementBasePrice,
            MeterPerSecondBasePrice = gameData.MeterPerSecondBasePrice,
            //Set new values //SHOULD BE DONE BY A ShopSystem
            CurrentMoney = gameData.CurrentMoney - (gameData.MeterPerSecondBasePrice * gameData.BaseMetersPerSecond),
            BaseMetersPerSecond = gameData.BaseMetersPerSecond + 1,
        });

        gameData = GetGameDataComponent();//GetComponentData is called again because it was updated

        UpdateTexts(gameData);
        UpdateButtonsState(gameData);
    }

    private GameData GetGameDataComponent()
    {
        EntityQuery query = entityManager.CreateEntityQuery(typeof(GameData));
        if (query.IsEmptyIgnoreFilter) return new GameData();
        return query.GetSingleton<GameData>();
    }

    private void UpdateButtonsState(GameData gameData)
    {
        BuySwarmElementButton.interactable = gameData.CurrentMoney >= gameData.SwarmElementBasePrice * gameData.SwarmQuantity;
        BuyMeterPerSecondButton.interactable = gameData.CurrentMoney >= gameData.MeterPerSecondBasePrice * gameData.BaseMetersPerSecond;
    }
    #endregion
}
