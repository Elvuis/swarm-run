﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Unity.Entities;

public class UIPage_GameOver : UIPage
{
    #region Variables
    [SerializeField] protected TextMeshProUGUI metersTraveledText;
    [SerializeField] protected TextMeshProUGUI moneyGainedText;

    EntityManager entityManager;
    #endregion

    #region MonoBehaviour
    private void Awake()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
    }

    private void OnEnable()
    {
        UpdateTexts();
    }

    #endregion

    #region Methods
    public void PlayAgain()
    {
        UIManager.singleInstance.SetPage<UIPage_Game>();
        EventManager.StartingGame?.Invoke();
    }

    public void MainMenu()
    {
        UIManager.singleInstance.SetPage<UIPage_MainMenu>();
    }

    public void UpdateTexts()
    {
        EntityQuery query = entityManager.CreateEntityQuery(typeof(GameData));
        if (query.IsEmptyIgnoreFilter) return; //On Play, this method is called from OnEnable but it is useless because this page will be disabled instantly;
        int moneyRewardPerMeter = query.GetSingleton<GameData>().MoneyRewardPerMeter;

        query = entityManager.CreateEntityQuery(typeof(MetersTraveledData));
        float metersTraveled = query.GetSingleton<MetersTraveledData>().Value;

        metersTraveledText.text = ((int)metersTraveled).ToString();
        moneyGainedText.text = ((int)metersTraveled * moneyRewardPerMeter).ToString() + "$";
    }
    #endregion
}
