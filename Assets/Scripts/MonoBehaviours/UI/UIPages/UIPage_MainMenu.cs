﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

public class UIPage_MainMenu : UIPage
{
    public void Play()
    {
        UIManager.singleInstance.SetPage<UIPage_Game>();
        EventManager.StartingGame?.Invoke();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Shop()
    {
        UIManager.singleInstance.SetPage<UIPage_Shop>();
    }
}
