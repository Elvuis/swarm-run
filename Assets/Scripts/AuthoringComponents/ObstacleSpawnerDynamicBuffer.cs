﻿using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

/// <summary>
/// NB: THE OBJECT THAT USE THIS AUTHORINGCOMPONENT CANNOT BE CONVERTED USING A SUBSCENE.
/// </summary>
[DisallowMultipleComponent]
public class ObstacleSpawnerDynamicBuffer : MonoBehaviour, IConvertGameObjectToEntity, IDeclareReferencedPrefabs
{
    public GameObject[] obstaclePrefabs;

    public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
    {
        dstManager.AddBuffer<EntityBufferElement>(entity);
        DynamicBuffer<EntityBufferElement> buffer = dstManager.GetBuffer<EntityBufferElement>(entity);

        for(int i = 0; i<obstaclePrefabs.Length; i++)
            buffer.Add(new EntityBufferElement() { Value = conversionSystem.GetPrimaryEntity(obstaclePrefabs[i]) });
    }

    public void DeclareReferencedPrefabs(List<GameObject> referencedPrefabs)
    {
        for (int i = 0; i < obstaclePrefabs.Length; i++)
            referencedPrefabs.Add(obstaclePrefabs[i]);
    }
}
